//
//  JoggingEntryViewModel.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/15/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import Foundation

struct JoggingEntryViewModel {
    
    private static let metersPerSecondToKilometersPerHourCoefficient = 3.6;
    private static let metersPerSecondToFeetPerSecondCoefficient = 3.2808399;
    private static let metersPerSecondToMilesPerHourCoefficient = 2.23693629;
    
    private static let dateFormatter: NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.timeStyle = .NoStyle
        formatter.dateStyle = .MediumStyle
        return formatter
        }()
    
    private static let numberFormatter: NSNumberFormatter = {
        let formatter = NSNumberFormatter()
        formatter.locale = NSLocale.currentLocale()
        formatter.numberStyle = .DecimalStyle
        formatter.maximumSignificantDigits = 2
        formatter.usesSignificantDigits = true
        return formatter
    }()
    private static let lengthFormatter = NSLengthFormatter()
    
    let date: String
    let duration: String
    let distance: String
    let averageSpeed: String
    
    init(joggingEntry: JoggingEntry) {
        if let joggingDate = joggingEntry.date {
            date = JoggingEntryViewModel.dateFormatter.stringFromDate(joggingDate)
        } else {
            date = ""
        }
        duration = JoggingEntryViewModel.stringWithDuration(Int(joggingEntry.duration))
        distance = JoggingEntryViewModel.lengthFormatter.stringFromMeters(Double(joggingEntry.distance))
        averageSpeed = JoggingEntryViewModel.stringWithSpeed(joggingEntry.distance / joggingEntry.duration)
    }
    
    static func durationStringWithHours(hours: Int, minutes: Int) -> String {
        let hoursText: String
        switch hours {
        case 0: hoursText = ""
        case 1: hoursText = "1 hour"
        default: hoursText = "\(hours) hours"
        }
        
        let minutesText: String
        switch minutes {
        case 0: minutesText = ""
        case 1: minutesText = "1 minute"
        default: minutesText = "\(minutes) minutes"
        }
        
        return [hoursText, minutesText].filter { !$0.isEmpty }.joinWithSeparator(" ")
    }
    
    private static func stringWithDuration(duration: Int) -> String {
        return durationStringWithHours(duration / 3600, minutes: duration / 60 % 60)
    }

    static func stringWithSpeed(speed: Double) -> String {
        if NSLocale.currentLocale().usesMetricSystem {
            return stringWithSpeedInMetric(speed)
        } else {
            return stringWithSpeedInImperial(speed)
        }
    }
    
    private static func stringWithSpeedInMetric(speed: Double) -> String {
        let metersPerSecondSpeed = speed
        let kilometersPerHourSpeed = speed * metersPerSecondToKilometersPerHourCoefficient
        if kilometersPerHourSpeed > 1 {
            return stringWithSpeed(kilometersPerHourSpeed, unit: "km/h")
        } else {
            return stringWithSpeed(metersPerSecondSpeed, unit: "m/s")
        }
    }
    
    private static func stringWithSpeedInImperial(speed: Double) -> String {
        let feetPerSecondSpeed = speed * metersPerSecondToFeetPerSecondCoefficient
        let milesPerHourSpeed = speed * metersPerSecondToMilesPerHourCoefficient
        if milesPerHourSpeed > 1 {
            return stringWithSpeed(milesPerHourSpeed, unit: "mph")
        } else {
            return stringWithSpeed(feetPerSecondSpeed, unit: "ft/s")
        }

    }
    
    private static func stringWithSpeed(speed: Double, unit: String) -> String {
        if let stringSpeed = numberFormatter.stringFromNumber(NSNumber(double: speed)) {
            return stringSpeed + " " + unit
        } else {
            return ""
        }
    }
}
