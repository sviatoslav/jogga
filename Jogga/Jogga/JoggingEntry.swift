//
//  JoggingEntry.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/18/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import Foundation
import CoreData

class JoggingEntry: NSManagedObject {

    @NSManaged var date: NSDate?
    @NSManaged var distance: Double
    @NSManaged var duration: Double
    @NSManaged var id: String
    @NSManaged var week: String?
    @NSManaged var user: User?

}
