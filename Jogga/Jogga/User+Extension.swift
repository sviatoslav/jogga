//
//  User+Extension.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/18/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import MagicalRecord

extension User {
    class var currentUser: User? {
        return User.MR_findFirstByAttribute("id", withValue: Settings.appSettings.currentUserID!)
    }
    
    var isManager: Bool {
        return role == "manager"
    }
}