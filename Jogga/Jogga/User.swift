//
//  User.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/18/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import Foundation
import CoreData

class User: NSManagedObject {

    @NSManaged var id: String
    @NSManaged var name: String?
    @NSManaged var role: String?
    @NSManaged var joggingEntries: NSSet

}
