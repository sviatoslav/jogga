//
//  NSManagedObject+MagicalRecordFix.h
//
//  Created by Sviatoslav Yakymiv on 11/3/14.
//  Copyright (c) 2014 Sviatoslav Yakymiv. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (MagicalRecordFix)

@end
