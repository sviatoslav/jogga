//
//  JoggingEntryViewController.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/15/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import UIKit
import MagicalRecord
import SVProgressHUD

class JoggingEntryViewController: UITableViewController, UITextFieldDelegate {
    
    private static let dateFormatter: NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.timeStyle = .NoStyle
        formatter.dateStyle = .MediumStyle
        return formatter
    }()
    
    private static let distanceFormatter = NSNumberFormatter()
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var actionBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var dateTextField: UITextField?
    @IBOutlet weak var distanceTextField: UITextField?
    @IBOutlet weak var durationTextField: UITextField?
    
    var joggingEntry: JoggingEntry?
    
    private lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.backgroundColor = UIColor.whiteColor()
        picker.datePickerMode = .Date
        picker.addTarget(self, action: "datePickerDidChangeValue:", forControlEvents: .ValueChanged)
        return picker
    }()
    
    private lazy var durationPicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.backgroundColor = UIColor.whiteColor()
        picker.datePickerMode = .CountDownTimer
        picker.addTarget(self, action: "datePickerDidChangeValue:", forControlEvents: .ValueChanged)
        return picker
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateTextField?.inputView = datePicker
        self.durationTextField?.inputView = durationPicker
        self.dateTextField?.placeholder = self.dynamicType.dateFormatter.stringFromDate(NSDate())
        if let joggingEntry = self.joggingEntry {
            if let date = joggingEntry.date {
                self.datePicker.date = date
                self.datePickerDidChangeValue(self.datePicker)
            }
            self.durationPicker.countDownDuration = joggingEntry.duration
            self.datePickerDidChangeValue(self.durationPicker)
            self.distanceTextField?.text = "\(joggingEntry.distance)"
        }
    }
    
    func datePickerDidChangeValue(picker: UIDatePicker) {
        switch picker {
        case self.datePicker:
            self.dateTextField?.text = self.dynamicType.dateFormatter.stringFromDate(picker.date)
        case self.durationPicker:
            let components = NSCalendar.currentCalendar()
                    .components([NSCalendarUnit.Hour, NSCalendarUnit.Minute], fromDate: picker.date)
            self.durationTextField?.text = JoggingEntryViewModel.durationStringWithHours(components.hour,
                    minutes: components.minute)
        default: break
        }
    }
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func save(sender: UIBarButtonItem) {
        if validate() {
            SVProgressHUD.show()
            let completion = { (joggingEntry: JoggingEntry?, errors: [ErrorType]) -> Void in
                if !errors.isEmpty {
//                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                } else {
                    SVProgressHUD.dismiss()
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
            }
            let date = self.datePicker.date
            let distance = self.enteredDistance
            let duration = self.durationPicker.countDownDuration
            if let joggingEntry = self.joggingEntry {
                UpdateJoggingEntryOperation(joggingEntryID: joggingEntry.id,
                    date: date,
                    distance: distance,
                    duration: duration,
                    completion: completion).enqueue()

            } else {
                CreateJoggingEntryOperation(date: date,
                    distance: distance,
                    duration: duration,
                    completion: completion).enqueue()
            }
        }
    }
    
    private var enteredDistance: Double {
        return self.dynamicType.distanceFormatter.numberFromString(distanceTextField?.text ?? "")?.doubleValue ?? 0
    }
    
    private func validate() -> Bool {
        var errors: [String] = []
        if (dateTextField?.text ?? "").characters.count <= 0 {
            errors.append("Invalid date")
        }
        if (durationTextField?.text ?? "").characters.count <= 0 {
            errors.append("Invalid duration")
        }
        if enteredDistance <= 0 {
            errors.append("Invalid distance")
        }
        
        if errors.count > 0 {
            UIAlertView(title: "Validation failed",
                message: errors.joinWithSeparator("\n"),
                delegate: nil, cancelButtonTitle: "OK")
                .show()
            return false
        }
        return true
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.userInteractionEnabled = false
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        textField.userInteractionEnabled = true
        return true
    }
}
