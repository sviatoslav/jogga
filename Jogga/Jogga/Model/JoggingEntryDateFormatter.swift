//
//  JoggingEntryDateFormatter.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/18/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import Foundation

class JoggingEntryDateFormatter: NSDateFormatter {
    
    override init() {
        super.init()
        initDateFormat()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initDateFormat()
    }
    
    func initDateFormat() {
        dateFormat = "yyyy-MM-dd"
    }
}
