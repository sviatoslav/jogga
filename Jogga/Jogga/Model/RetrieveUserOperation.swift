//
//  RetrieveUserOperation.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/18/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import Foundation
import APIOperation

class RetrieveUserOperation: APIOperation<User> {
    
    private let userID: NSString
    
    init(userID: String, completion: Completion? = nil) {
        self.userID = userID
        super.init(method: .GET, path: "users/\(userID).json?auth=\(Settings.appSettings.auth)", completion: completion)
    }
}
