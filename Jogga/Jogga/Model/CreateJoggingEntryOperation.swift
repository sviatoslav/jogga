//
//  CreateJoggingEntryOperation.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/18/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import APIOperation

class CreateJoggingEntryOperation: APIOperation<JoggingEntry> {
    
    private let joggingParameters: [String: AnyObject]
    
    init(date: NSDate, distance: Double, duration: Double, completion: Completion) {
        joggingParameters = ["date": JoggingEntryDateFormatter().stringFromDate(date),
                            "duration": duration,
                            "distance": distance,
                            "user_id": Settings.appSettings.currentUserID!]
        super.init(method: .POST, path: "joggings.json?auth=\(Settings.appSettings.auth)",
            parameters: joggingParameters, completion: completion)
        
        self.responsePreprocessingOperation = MappingOperation<AnyObject, AnyObject> {
            if let params = $0 as? [String: String] {
                var result = self.joggingParameters
                result["id"] = params["name"]
                return result
            }
            return $0
        }

    }
}
