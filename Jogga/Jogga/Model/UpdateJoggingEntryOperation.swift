//
//  UpdateJoggingEntryOperation.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/18/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import APIOperation

class UpdateJoggingEntryOperation: APIOperation<JoggingEntry> {
    
    private let joggingEntryID: String
    
    init(joggingEntryID: String, date: NSDate, distance: Double, duration: Double, completion: Completion? = nil) {
        self.joggingEntryID = joggingEntryID
        super.init(method: .PATCH, path: "joggings/\(joggingEntryID)", parameters:
            ["date": JoggingEntryDateFormatter().stringFromDate(date),
            "duration": duration,
            "distance": distance], completion: completion)
        
        self.responsePreprocessingOperation = MappingOperation<AnyObject, AnyObject> {
            if let params = $0 as? [String: AnyObject] {
                var result = params
                result["id"] = joggingEntryID
                return result
            }
            return $0
        }

    }
}
