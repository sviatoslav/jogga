//
//  LogoutOperation.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/10/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import FBSDKLoginKit
import MagicalRecord
import APIOperation

let LogoutOperationDidFinishNotification = "LogoutOperationDidFinishNotification"

class LogoutOperation: NSOperation, Enqueueable {
    
    override func main() {
        clearCookies()
        cancelAPIOperations()
        FBSDKLoginManager().logOut()
        Settings.appSettings.currentUserID = nil
        Settings.appSettings.token = nil
        MagicalRecord.saveWithBlockAndWait { (moc) -> Void in
            User.MR_truncateAllInContext(moc)
            JoggingEntry.MR_truncateAllInContext(moc)
        }
        NSNotificationCenter.defaultCenter().postNotificationName(LogoutOperationDidFinishNotification, object: self)
    }
    
    private func clearCookies() {
        let cookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
        for cookie in cookieStorage.cookies ?? [] {
            cookieStorage.deleteCookie(cookie)
        }
    }
    
    private func cancelAPIOperations() {
        APIOperation<Any>.cancelAll()
    }
}
