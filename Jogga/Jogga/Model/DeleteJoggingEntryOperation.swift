//
//  DeleteJoggingEntryOperation.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/18/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import APIOperation

class DeleteJoggingEntryOperation: APIOperation<NSNull> {
    init(joggingEntryID: String, completion: Completion? = nil) {
        super.init(method: .DELETE, path: "joggings/\(joggingEntryID).json?auth=\(Settings.appSettings.auth)",
            completion: completion)
    }
}
