//
//  CreateUserOperation.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/18/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import Operations
import APIOperation

class UpdateUserOperation: APIOperation<User>, InjectionOperationType {
    let userID: NSString
    init(userID: String, name: String, completion: Completion? = nil) {
        self.userID = userID
        super.init(method: .PATCH, path: "users/\(userID).json?auth=\(Settings.appSettings.auth)",
            parameters: ["name": name, "id": userID], completion: completion)
    }
}
