//
//  RetrieveJoggingEntriesOperation.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/18/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import APIOperation

class RetrieveJoggingEntriesOperation: APIOperation<[JoggingEntry]> {
    
    init(userID: String? = nil, completion: Completion? = nil) {
        let path: String
        if let userID = userID {
            path = "joggings.json?auth=\(Settings.appSettings.auth)&orderBy=%22user_id%22&equalTo=%22\(userID)%22"
        } else {
            path = "joggings.json?auth=\(Settings.appSettings.auth)"
        }
        super.init(method: .GET, path: path, completion: completion)
        
        self.responsePreprocessingOperation = MappingOperation<AnyObject, AnyObject> {
            if let dict = $0 as? [String: [String: AnyObject]] {
                var result: [[String: AnyObject]] = []
                for (key, value) in dict {
                    var item = value
                    item["id"] = key
                    result.append(item)
                }
                return result
            }
            return $0
        }
    }
}
