//
//  Settings.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/4/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import Firebase

class Settings {
    private static let bundleID = NSBundle.mainBundle().bundleIdentifier ?? ""
    private static let currentUserIDKey = bundleID + "CurrentUserID"
    private static let tokenKey = bundleID + "Token"
    
    static let appSettings = Settings(userDefaults: NSUserDefaults.standardUserDefaults())
    
    private init(userDefaults: NSUserDefaults? = nil) {
        self.userDefaults = userDefaults
    }
    
    private let userDefaults: NSUserDefaults?
    
    var currentUserID: String? {
        get {
            return self.objectForKey(Settings.currentUserIDKey) as? String
        }
        set {
            self.setObject(newValue, forKey: Settings.currentUserIDKey)
        }
    }
    
    var token: String? {
        get {
            return self.objectForKey(Settings.tokenKey) as? String
        }
        set {
            self.setObject(newValue, forKey: Settings.tokenKey)
        }
    }
    
    private func setObject(object: AnyObject?, forKey key: String) {
        if let object = object {
            userDefaults?.setObject(object, forKey: key)
        } else {
            userDefaults?.removeObjectForKey(key)
        }
    }
    
    private func objectForKey(key: String) -> AnyObject? {
        return self.userDefaults?.objectForKey(key)
    }
    
    var auth: String {
        return token ?? "null"
    }
    
    var isUserLoggedIn: Bool {
        get {
            return self.token != nil
        }
    }
    
    let baseURL = NSURL(string: "https://intense-torch-3695.firebaseio.com")!
}
