//
//  LoginOperation.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/17/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import Operations
import Firebase

enum FirebaseAuthOperationError: ErrorType {
    case AuthFailed
}

class FirebaseAuthOperation: Operation, ResultOperationType {
    
    typealias Completion = (FAuthData?, [ErrorType]) -> Void
    private let completion: Completion?
    private let facebookAccessToken: String
    
    private(set) var result: FAuthData?
    
    init(facebookAccessToken: String, completion: Completion? = nil) {
        self.facebookAccessToken = facebookAccessToken
        self.completion = completion
        super.init()
    }

    override func execute() {
        let firebase = Firebase(url: Settings.appSettings.baseURL.absoluteString)
        firebase.authWithOAuthProvider("facebook", token: facebookAccessToken,
            withCompletionBlock: { (error, authData) -> Void in
                if self.cancelled {
                    return
                }
                let errors: [ErrorType]
                defer {
                    if !self.cancelled {
                        self.completion?(self.result, self.errors)
                        self.finish(errors)
                    }
                }
                guard let authData = authData else {
                    errors = [FirebaseAuthOperationError.AuthFailed]
                    return
                }
                errors = []
                self.result = authData
                Settings.appSettings.token = authData.token
                Settings.appSettings.currentUserID = authData.uid
        })
    }
}
