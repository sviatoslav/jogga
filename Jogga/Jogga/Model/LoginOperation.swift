//
//  LoginOperation.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 2/8/16.
//  Copyright © 2016 Sviatoslav Yakymiv. All rights reserved.
//

import Operations
import APIOperation

class LoginOperation: GroupOperation, ResultOperationType, APIOperationEnqueueable {
    typealias Completion = (User?, [ErrorType]) -> Void
    private(set) var result: User?
    private let completion: Completion?
    
    init(facebookAccessToken: String, completion: Completion? = nil) {
        self.completion = completion
        let firebaseAuthOperation = FirebaseAuthOperation(facebookAccessToken: facebookAccessToken)
        super.init(operations: [firebaseAuthOperation])
    }
    
    override func operationDidFinish(operation: NSOperation, withErrors errors: [ErrorType]) {
        if let firebaseAuthOperation = operation as? FirebaseAuthOperation,
            authData = firebaseAuthOperation.result {
            let updateUserOperation = UpdateUserOperation(userID: authData.uid,
                name: (authData.providerData["displayName"] as? String) ?? "", completion: self.completion)
            self.addOperation(updateUserOperation)
        }
    }
    
    override func finished(errors: [ErrorType]) {
        completion?(self.result, errors)
    }
}
