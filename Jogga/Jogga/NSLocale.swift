//
//  NSLocale.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/15/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import Foundation

extension NSLocale {
    var usesMetricSystem: Bool {
        return ((self.objectForKey(NSLocaleUsesMetricSystem) as? Bool) ?? true)
    }
}