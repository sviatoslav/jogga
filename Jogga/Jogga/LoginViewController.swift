//
//  ViewController.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/13/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import FBSDKLoginKit
import Firebase
import SVProgressHUD

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var fbLoginButton: FBSDKLoginButton!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.fbLoginButton.hidden = Settings.appSettings.isUserLoggedIn
        if Settings.appSettings.isUserLoggedIn {
            self.loadCurrentUser()
        }
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!,
        error: NSError!) {
        
        SVProgressHUD.show()
        self.fbLoginButton.hidden = true
        LoginOperation(facebookAccessToken: result.token.tokenString) { (user, errors) -> Void in
            if user != nil {
                self.loadCurrentUser()
            } else {
                //TODO:
//                add error handling
            }
        }.enqueue()
    }
    
    private func loadCurrentUser() {
        SVProgressHUD.show()
        guard let id = Settings.appSettings.currentUserID else {
            self.logout()
            return
        }
        RetrieveUserOperation(userID: id) {(user, error) -> Void in
            //TODO: show error
//                if self.showError(error) { return }
            if User.currentUser != nil {
                SVProgressHUD.dismiss()
                self.performSegueWithIdentifier("show main screen", sender: nil)
            } else {
               self.logout()
            }
        }.enqueue()
    }
    
    private func logout() {
        LogoutOperation().enqueue()
        self.fbLoginButton.hidden = false
    }
    
    private func showError(error: NSError?) -> Bool {
        if let error = error {
            fbLoginButton.hidden = false
            SVProgressHUD.showErrorWithStatus(error.localizedDescription)
        }
        return error != nil
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
    }
    
}

