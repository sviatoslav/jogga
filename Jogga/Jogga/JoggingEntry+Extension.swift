//
//  JoggingEntry+Extension.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/18/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import SwiftDate

extension JoggingEntry {
    override func didImport(object: AnyObject?) {
        if object is [String: AnyObject] {
            let firstDayOfWeek = self.date!.inDefaultRegion().startOf(.WeekOfYear).absoluteTime
            let components = NSDateComponents()
            components.day = 6
            let lastDayOfWeek = NSCalendar.currentCalendar().dateByAddingComponents(components,
                toDate: firstDayOfWeek, options: []) ?? NSDate()
            let formatter = JoggingEntryDateFormatter()
            self.week = formatter.stringFromDate(firstDayOfWeek) + " - " + formatter.stringFromDate(lastDayOfWeek)
        }
    }
}