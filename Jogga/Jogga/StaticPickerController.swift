//
//  StaticPickerDataSource.swift
//  Zenna
//
//  Created by Sviatoslav Yakymiv on 4/22/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import UIKit

protocol StaticPickerControllerDelegate: class {
    func staticPickerController(controller: StaticPickerController,
        didSelectItemAtIndex index: Int, inComponent component: Int)
}

class StaticPickerController: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    var items: [[String]]?
    var delegate: StaticPickerControllerDelegate?
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return items?.count ?? 0;
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items?[component].count ?? 0
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return items?[component][row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        delegate?.staticPickerController(self, didSelectItemAtIndex: row, inComponent: component)
    }
}
