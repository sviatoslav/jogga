//
//  JoggingEntryTableViewCell.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/15/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import UIKit

class JoggingEntryTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel?
    @IBOutlet weak var durationLabel: UILabel?
    @IBOutlet weak var distanceLabel: UILabel?
    @IBOutlet weak var averageSpeedLabel: UILabel?

}
