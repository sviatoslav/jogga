//
//  AppDelegate.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/13/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import MagicalRecord
import APIOperation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication,
            didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        NSTimeZone.setDefaultTimeZone(NSTimeZone(forSecondsFromGMT: 0))
        MagicalRecord.setupAutoMigratingCoreDataStack()
        APIOperation<Any>.defaultConfiguration = Configuration(baseURL: Settings.appSettings.baseURL,
            requestOperationFactory: AlamofireRequestOperationFactory(),
            responseProcessingOperationFactory: MRResponseProcessingOperationFactory())
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }

    func applicationDidBecomeActive(application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?,
            annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
}

