//
//  JoggingEntriesTableViewController.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/15/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import UIKit
import MagicalRecord
import SVProgressHUD

class JoggingEntriesTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {

    private lazy var fetchedResultsController: NSFetchedResultsController = {
        JoggingEntry.MR_fetchAllSortedBy("date", ascending: true, withPredicate: nil, groupBy: "week", delegate: nil)
    }()
    private let rowAnimation = UITableViewRowAnimation.Fade
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = true
        self.fetchedResultsController.delegate = self
        
        self.startDateTextField?.inputView = self.startDatePicker
        self.endDateTextField?.inputView = self.endDatePicker
    }
    
    @IBAction func logout(sender: UIBarButtonItem) {
        LogoutOperation().enqueue()
        dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if User.currentUser?.isManager ?? false {
            RetrieveJoggingEntriesOperation().enqueue()
        } else {
            RetrieveJoggingEntriesOperation(userID: User.currentUser!.id).enqueue()
        }
    }
    
    // MARK: - Fetched results controller delegate
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject,
            atIndexPath indexPath: NSIndexPath?,
            forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: rowAnimation)
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: rowAnimation)
        case .Update:
            tableView.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: .None)
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: rowAnimation)
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: rowAnimation)
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo,
            atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: rowAnimation)
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: rowAnimation)
        default: ()
        }
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        let result = fetchedResultsController.sections?.count ?? 0
        return result
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].objects?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let week = fetchedResultsController.sections![section].name
        let entries = JoggingEntry.MR_findByAttribute("week", withValue: week)?.reduce(NSMutableArray(), combine: {
            $0.0.addObject($0.1)
            return $0.0
        })
        let distanceSum = (entries?.valueForKeyPath("@sum.distance") as? NSNumber)?.doubleValue ?? 0
        let durationSum = (entries?.valueForKeyPath("@sum.duration") as? NSNumber)?.doubleValue ?? 0
        let averageSpeed = distanceSum / durationSum
        
        return "\(week): \(JoggingEntryViewModel.stringWithSpeed(averageSpeed))"
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("jogging entry cell",
                forIndexPath: indexPath)as! JoggingEntryTableViewCell
        
        if let joggingEntry = fetchedResultsController.objectAtIndexPath(indexPath) as? JoggingEntry {
            let joggingEntryViewModel = JoggingEntryViewModel(joggingEntry: joggingEntry)
            cell.distanceLabel?.text = joggingEntryViewModel.distance
            cell.dateLabel?.text = joggingEntryViewModel.date
            cell.durationLabel?.text = joggingEntryViewModel.duration
            cell.averageSpeedLabel?.text = joggingEntryViewModel.averageSpeed
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle,
            forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle != .Delete {
            return
        }
        if let joggingEntry = self.fetchedResultsController.objectAtIndexPath(indexPath) as? JoggingEntry {
            SVProgressHUD.show()
            DeleteJoggingEntryOperation(joggingEntryID: joggingEntry.id, completion: { (_, errors) -> Void in
                if !errors.isEmpty {
                    //TODO: Add error handling
//                    SVProgressHUD.showErrorWithStatus(error.localizedDescription)
                } else {
                    SVProgressHUD.dismiss()
                    MagicalRecord.saveWithBlockAndWait({ (managedObjectContext) -> Void in
                        joggingEntry.MR_deleteEntityInContext(managedObjectContext)
                    })
                }
            }).enqueue()
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier != "edit jogging entry" {
            return
        }
        if let cell = sender as? UITableViewCell,
            let indexPath = self.tableView.indexPathForCell(cell),
            let joggingEntry = fetchedResultsController.objectAtIndexPath(indexPath) as? JoggingEntry,
            let navController = segue.destinationViewController as? UINavigationController,
            let entryViewController = navController.topViewController as? JoggingEntryViewController
            {
            entryViewController.joggingEntry = joggingEntry
        }
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
    }
    
    //MARK: - Filtering
    
    @IBOutlet weak var startDateTextField: UITextField?
    
    @IBOutlet weak var endDateTextField: UITextField?
    
    @IBAction func clearStartDateTextField(sender: AnyObject) {
        self.startDateTextField?.text = ""
    }
    @IBAction func clearEndDateTextField(sender: AnyObject) {
        self.endDateTextField?.text = ""
    }
    @IBAction func filter(sender: AnyObject) {
        let isStartDateEmpty = self.startDateTextField?.text?.isEmpty ?? true
        let isEndDateEmpty = self.endDateTextField?.text?.isEmpty ?? true
        let predicate = NSPredicate(format: "date >= %@ && date <= %@", argumentArray:
            [isStartDateEmpty ? NSDate.distantPast() : self.startDatePicker.date,
                isEndDateEmpty ? NSDate.distantFuture() : self.endDatePicker.date])
        self.fetchedResultsController.fetchRequest.predicate = predicate
        NSFetchedResultsController.deleteCacheWithName("Root")
        do {
            try self.fetchedResultsController.performFetch()
        } catch _ {
        }
        tableView.reloadData()
    }
    
    private lazy var startDatePicker: UIDatePicker = {
        return self.createDatePicker()
    }()
    
    private lazy var endDatePicker: UIDatePicker = {
        return self.createDatePicker()
    }()
    
    func createDatePicker() -> UIDatePicker {
        let picker = UIDatePicker()
        picker.backgroundColor = UIColor.whiteColor()
        picker.datePickerMode = .Date
        picker.addTarget(self, action: "datePickerDidChangeValue:", forControlEvents: .ValueChanged)
        return picker
    }
    
    private let dateFormatter = JoggingEntryDateFormatter()
    
    func datePickerDidChangeValue(datePicker: UIDatePicker) {
        if datePicker == startDatePicker {
            self.startDateTextField?.text = dateFormatter.stringFromDate(datePicker.date)
        } else {
            self.endDateTextField?.text = dateFormatter.stringFromDate(datePicker.date)
        }
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.userInteractionEnabled = false
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        textField.userInteractionEnabled = true
        return true
    }
}
