//
//  LoginTest.swift
//  Jogga
//
//  Created by Sviatoslav Yakymiv on 8/19/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import XCTest
import Firebase

class LoginTest: XCTestCase {

    private let facebookToken = "CAAB0bXZAzjycBAL0ZCYdrLW3sgmHTuJGQOl4zsneyauivn87ZBW0ma85j42SkZBuOsgV3kosS0mJZB0JIFz8PrUpAFmwLdn8RamIneOQlU8xrc3Tj6HEYrkxQMAOSzgUgMdBv2di3J5bxvgIB3NNK62pucqbAvZBgZCbon5ZB85JzfaBTeczAtRq0LkVue24HHEZD"
    private let firebase = Firebase(url: "https://intense-torch-3695.firebaseio.com")
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        firebase.unauth()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testLogin() {
        let expectation = expectationWithDescription("Successful login expectation")
        firebase.authWithOAuthProvider("facebook", token: facebookToken) { (error, authData) -> Void in
            XCTAssertNotNil(authData.token, "Firebase token is nil")
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(30, handler: nil)
    }
}
