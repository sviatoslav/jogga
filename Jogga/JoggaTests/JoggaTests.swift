//
//  JoggaTests.swift
//  JoggaTests
//
//  Created by Sviatoslav Yakymiv on 8/13/15.
//  Copyright (c) 2015 Sviatoslav Yakymiv. All rights reserved.
//

import UIKit
import XCTest
import Firebase
import Foundation

class JoggaTests: XCTestCase {
    
    private var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2IjowLCJkIjp7InVpZCI6ImZhY2Vib29rOjkwOTQ3OTY2MjQ1NjgyMSIsInByb3ZpZGVyIjoiZmFjZWJvb2sifSwiaWF0IjoxNDQwMDE4OTI0fQ.ys50Oo_0mKKVE_63zE9QnnKS6OATXdtU5WZK27jNg9g"
    private var uid = "facebook:909479662456821"
    
    override func setUp() {
        super.setUp()
    }
    
    func testGetCurrentUser() {
        let result = performNetworkRequest("https://intense-torch-3695.firebaseio.com/users/\(uid).json?auth=\(token)", method: "GET")
        XCTAssertNil(result.2, result.2?.description ?? "")
        XCTAssertNotNil(result.0 as? [String: AnyObject], "Invalid response")
    }
    
    func testUpdateCurrentUser() {
        let result = performNetworkRequest("https://intense-torch-3695.firebaseio.com/users/\(uid).json?auth=\(token)", method: "PATCH", parameters: ["test": "test"])
        XCTAssertNil(result.2, result.2?.description ?? "")
        XCTAssertEqual((result.0 as? [String: String])!, ["test": "test"], "")
    }
    
    func testDelete() {
        let result = performNetworkRequest("https://intense-torch-3695.firebaseio.com/users/\(uid)/test.json?auth=\(token)", method: "DELETE")
        XCTAssertNil(result.2, result.2?.description ?? "")
        XCTAssertNotNil(result.0 as? NSNull, "Invalid response")
    }
    
    func testNonAuthorized() {
        let result = performNetworkRequest("https://intense-torch-3695.firebaseio.com/users/\(uid).json", method: "GET")
        XCTAssertEqual(result.2?.code ?? -1, -1012, "Invalid response")
    }
    
    private func performNetworkRequest(urlString: String, method: String, parameters: [String: AnyObject]? = nil) -> (AnyObject?, NSHTTPURLResponse?, NSError?) {
        let url = NSURL(string: urlString)
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = method
        if let parameters = parameters {
            let _ = try? request.HTTPBody = NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        }
        var response: NSURLResponse?
        var error: NSError?
        let data = try? NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
        var object: AnyObject? = nil
        if let data = data {
            object = try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)
        }
        return (object, response as? NSHTTPURLResponse, error)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
}
